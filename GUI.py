# The code for changing pages was derived from: http://stackoverflow.com/questions/7546050/switch-between-two-frames-in-tkinter
# License: http://creativecommons.org/licenses/by-sa/3.0/

import matplotlib
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import tkinter as tk
from tkinter import ttk
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb
import statsmodels.api as sm
import scipy
from sklearn.linear_model import LinearRegression
from tkinter.ttk import *
from PIL import Image
from PIL import ImageTk
from win32api import GetSystemMetrics

matplotlib.use("TkAgg")

LARGE_FONT = ("Verdana", 12)

__author__ = "zahra jalili"

"""
------------------------------------------------------------------------
                 Functionality Classes and Methods
------------------------------------------------------------------------
"""


class Read:
    @staticmethod
    def read_csv_file(filename):
        DF1 = pd.read_csv(filename)
        return DF1


class Delete:
    @staticmethod
    def del_col(df, deletedList):
        # deleting some of the columns of heart
        for item in deletedList:
            del df[item]
        return df


class Rename:
    @staticmethod
    def rename_col(df, rename_dict):
        # Rename a column name for heart
        for item in rename_dict:
            df = df.rename(columns={item: rename_dict[item]})
        return df


class Join:
    # join two df
    @staticmethod
    def changet_to_type(df, col_list, type):
        # change the type of column Year for ech datafarmes
        for item in col_list:
            df[item] = df[item].astype(type)
        return df

    @staticmethod
    def join_datasets(df1, df2, col_list):
        # use inner join for merging two datasets
        return pd.merge(left=df1, right=df2, left_on=col_list, right_on=col_list)


class OVERALL:

    ################## OVERALL
    @staticmethod
    def filter_hd(df):
        df1 = df[(df["Data_Value_Type"] == "Crude") & (df["Break_out"] == "Overall") &
                 (df["Category_x"] == "Cardiovascular Diseases") & (df["Topic_x"] == "Coronary Heart Disease")]

        return df1

    @staticmethod
    def filter_st(df):
        df1 = df[(df["Data_Value_Type"] == "Crude") & (df["Break_out"] == "Overall") &
                 (df["Category_x"] == "Cardiovascular Diseases") & (df["Topic_x"] == "Stroke")]

        return df1

    @staticmethod
    def filter_sm(df):
        df1 = df[((df["Year"] == "2011") | (df["Year"] == "2012") | (df["Year"] == "2013") | (df["Year"] == "2014")
                  | (df["Year"] == "2015")) & (df["Race"] == "All Races") & (df["Gender"] == "Overall")
                 ]
        return df1


class DataFrames:
    @staticmethod
    def dropna(df, column):
        df.dropna(subset=[column], inplace=True)
        return df

    @staticmethod
    def sort(df, column):
        df.sort_values(by=[column], inplace=True)
        return df

    @staticmethod
    def groupby_mean(df, columns_list):
        df = df.groupby(columns_list).mean()
        return df


class Plots:
    @staticmethod
    def plot_year(df, column, title, y_label, color):
        f = Figure(figsize=(5, 5), dpi=100)
        a = [f.add_subplot(511), f.add_subplot(512), f.add_subplot(513),
             f.add_subplot(514), f.add_subplot(515)]
        df[column].unstack(level=0).plot(kind='bar', subplots=True, legend=True,
                                         cmap=color,
                                         title=title,
                                         ax=a)
        # setting the y label for each plot
        count = 0
        for plot in a:
            plot.set_ylabel(y_label)
            if count != 4:
                plot.get_xaxis().set_visible(False)
            count += 1
        return f

    @staticmethod
    def plot_regression(df, x_column, y_column, title, y_label):
        # Create the scatter plot with a regression line
        f = Figure(figsize=(5, 5), dpi=100)
        a = f.add_subplot(111)
        sb.regplot(x=x_column, y=y_column,
                   data=df, scatter_kws={"color": "black"}, line_kws={"color": "red"}, ax=a)
        a.title.set_text(title)
        a.set_ylabel(y_label)
        return f

    @staticmethod
    def plot_regplot_sidebyside(df1, df2, x_column1, y_column1, x_column2, y_column2,
                                title1, title2, y_label):
        fig, axs = plt.subplots(ncols=2)
        sb.regplot(x=x_column1, y=y_column1,
                   data=df1[[x_column1, y_column1]], scatter_kws={"color": "blue"}, line_kws={"color": "green"},
                   ax=axs[0])
        axs[0].title.set_text(title1)
        # axs[0].set_ylim(1.5, 9)
        axs[0].set_xlim(6, 35)
        axs[0].set_ylabel(y_label)
        sb.regplot(x=x_column2, y=y_column2,
                   data=df2[[x_column2, y_column2]], scatter_kws={"color": "green"}, line_kws={"color": "red"},
                   ax=axs[1])
        axs[1].title.set_text(title2)
        # axs[1].set_ylim(1.5, 9)
        axs[1].set_xlim(6, 35)
        axs[1].set_ylabel(y_label)
        return fig

    @staticmethod
    def line_plot(df, x_column, y_column, title):
        sb.lineplot(df[x_column], df[y_column])
        plt.title(title)
        plt.show()

    @staticmethod
    def regression_slope(x, y):
        model = sm.OLS(y, x)
        results = model.fit()
        # results.params
        return results.summary()

    @staticmethod
    def regression_slope2(x, y):
        slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x.tolist(), y.tolist())
        # For retrieving the slope:
        return slope


class FEMALE_MALE:
    @staticmethod
    def filter_st_hd_fe_ma(df, topic_x, sex):
        df1 = df[(df["Data_Value_Type"] == "Crude") & (df["Break_out"] == sex) &
                 (df["Gender"] == sex) & (df["Topic_x"] == topic_x) & (df["Race"] == "All Races") &
                 (df["Age"] == "All Ages") & (df["Education"] == "All Grades") & (df["Response"] == "Current")]
        return df1


"""
------------------------------------------------------------------------
                        User Interface Classes and Methods
------------------------------------------------------------------------
"""


class SeaofBTCapp(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        tk.Tk.wm_title(self, "Effect of Smoking on Health")

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (
                StartPage, PageHeart, PageStroke, PageSmoke, PageSmokeHeart, PageSmokeStroke,
                PageSmokeHeartFemaleVsMale,
                PageSmokeStrokeFemaleVsMale):
            frame = F(container, self)

            self.frames[F] = frame

            frame.grid(row=0, column=0, sticky="nsew")
            # frame.pack()
        self.show_frame(StartPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()


class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        super(StartPage, self).__init__(parent, borderwidth=0, highlightthickness=0)
        self.canvas = tk.Canvas(self, width=width - 70, height=height - 80)
        self.canvas.pack()

        pil_img = Image.open("Data/img/heartsmoking.jpg")
        self.img = ImageTk.PhotoImage(pil_img.resize((width, height), Image.ANTIALIAS))
        self.bg = self.canvas.create_image(0, 0, anchor=tk.NW, image=self.img)

        ttk.Style().configure('green/black.TLabel', foreground='black', background='white', font=('Helvetica', 22))
        ttk.Style().configure('green/black.TButton', foreground='green', background='black', font=('Helvetica', 16))

        self.add(
            (ttk.Label(self, text="Press the following buttons to see the graphs", style='green/black.TLabel')), 20, 10)

        self.add(ttk.Button(self, text="Heart Disease in US (2011-2015)", style="green/black.TButton",
                            command=lambda: controller.show_frame(PageHeart)), 50, 70)

        self.add(ttk.Button(self, text="Stroke in US (2011-2015)", style="green/black.TButton",
                            command=lambda: controller.show_frame(PageStroke)), 50, 130)

        self.add(ttk.Button(self, text="Smoking in US (2011-2015)", style="green/black.TButton",
                            command=lambda: controller.show_frame(PageSmoke)), 50, 190)

        self.add(ttk.Button(self, text="Effect of Smoking on Heart Disease in All States of US",
                            style="green/black.TButton",
                            command=lambda: controller.show_frame(PageSmokeHeart)), 50, 250)

        self.add(ttk.Button(self, text="Effect of Smoking on Stroke in All States of US",
                            style="green/black.TButton",
                            command=lambda: controller.show_frame(PageSmokeStroke)), 50, 310)

        self.add(ttk.Button(self, text="Effect of  Smoking on Heart Disease for FEMALES vs MALES",
                            style="green/black.TButton",
                            command=lambda: controller.show_frame(PageSmokeHeartFemaleVsMale)), 50, 370)

        self.add(ttk.Button(self, text="Effect of Smoking on Stroke for FEMALES vs MALES",
                            style="green/black.TButton",
                            command=lambda: controller.show_frame(PageSmokeStrokeFemaleVsMale)), 50, 430)

    def add(self, widget, x, y):
        canvas_window = self.canvas.create_window(x, y, anchor=tk.NW, window=widget)
        return widget


class PageHeart(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Heart Disease in US (2011-2015)", font=LARGE_FONT)
        label.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text="Back to Home",
                             command=lambda: controller.show_frame(StartPage))
        button1.pack()

        f = PLOTS.plot_year(H_FILTERED, "Heart Rate (%)",
                            "Rate Of Heart Disease From 2011-2015 For US's states",
                            "HD(%)", color="winter")

        canvas = FigureCanvasTkAgg(f, self)
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

        # toolbar = NavigationToolbar2TkAgg(canvas, self)
        # toolbar.update()
        canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)


class PageStroke(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Stroke in US (2011-2015)", font=LARGE_FONT)
        label.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text="Back to Home",
                             command=lambda: controller.show_frame(StartPage))
        button1.pack()

        f = PLOTS.plot_year(ST_FILTERED, "Heart Rate (%)",
                            "Rate Of Stroke From 2011-2015 For US's states",
                            "Stroke(%)", color="spring")

        canvas = FigureCanvasTkAgg(f, self)
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

        # toolbar = NavigationToolbar2TkAgg(canvas, self)
        # toolbar.update()
        canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)


class PageSmoke(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Smoking in US (2011-2015)", font=LARGE_FONT)
        label.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text="Back to Home",
                             command=lambda: controller.show_frame(StartPage))
        button1.pack()

        f = PLOTS.plot_year(DF_SM2, 'Smoking Rate (%)',
                            "Rate Of Smoking From 2011-2015 For US's states", "Smoke(%)", color="summer")

        canvas = FigureCanvasTkAgg(f, self)
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

        # toolbar = NavigationToolbar2TkAgg(canvas, self)
        # toolbar.update()
        canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)


class PageSmokeHeart(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Effect of Smoking on Heart Disease in All States of US", font=LARGE_FONT)
        label.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text="Back to Home",
                             command=lambda: controller.show_frame(StartPage))
        button1.pack()

        f = PLOTS.plot_regression(H_FILTERED, 'Smoking Rate (%)', 'Heart Rate (%)',
                                  "Effect of Smoking on Heart Disease \n slope : 0.3894",
                                  "Heart Disease Percentage (%)")

        canvas = FigureCanvasTkAgg(f, self)
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

        # toolbar = NavigationToolbar2TkAgg(canvas, self)
        # toolbar.update()
        canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)


class PageSmokeStroke(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Effect of Smoking on Stroke in All States of US", font=LARGE_FONT)
        label.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text="Back to Home",
                             command=lambda: controller.show_frame(StartPage))
        button1.pack()

        f = PLOTS.plot_regression(ST_FILTERED, 'Smoking Rate (%)', 'Heart Rate (%)',
                                  "Effect of Smoking on Stroke \n slope: 0.2238 ",
                                  "Stroke Percentage(%)")

        canvas = FigureCanvasTkAgg(f, self)
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

        # toolbar = NavigationToolbar2TkAgg(canvas, self)
        # toolbar.update()
        canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)


class PageSmokeHeartFemaleVsMale(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Effect of  Smoking on Heart Disease for FEMALES vs MALES", font=LARGE_FONT)
        label.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text="Back to Home",
                             command=lambda: controller.show_frame(StartPage))
        button1.pack()

        f = PLOTS.plot_regplot_sidebyside(FEMALE_HEART, MALE_HEART, 'Smoking Rate (%)',
                                          'Heart Rate (%)',
                                          'Smoking Rate (%)', 'Heart Rate (%)',
                                          "Effect Of Smoking On Heart Disease In FEMALE (Slope: 0.1711)",
                                          "Effect Of Smoking On Heart Disease In MALE (Slope: 0.1697)",
                                          "Heart Disease Percentage(%)")

        canvas = FigureCanvasTkAgg(f, self)
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

        # toolbar = NavigationToolbar2TkAgg(canvas, self)
        # toolbar.update()
        canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)


class PageSmokeStrokeFemaleVsMale(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Effect of Smoking on Stroke for FEMALES vs MALES", font=LARGE_FONT)
        label.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text="Back to Home",
                             command=lambda: controller.show_frame(StartPage))
        button1.pack()

        f = PLOTS.plot_regplot_sidebyside(FEMALE_STROKE, MALE_STROKE, 'Smoking Rate (%)',
                                          'Heart Rate (%)',
                                          'Smoking Rate (%)', 'Heart Rate (%)',
                                          "Effect Of Smoking On Stroke In FEMALE (Slope: 0.1272)",
                                          "Effect Of Smoking On Stroke In MALE (Slope: 0.1088)",
                                          "Stroke Percentage(%)")

        canvas = FigureCanvasTkAgg(f, self)
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

        # toolbar = NavigationToolbar2TkAgg(canvas, self)
        # toolbar.update()
        canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)


READ = Read()
DF_HS = READ.read_csv_file("Data/heart.csv")
DF_SM = READ.read_csv_file("Data/smoking.csv")
print("The datasets are loaded.")

DELETE = Delete()
HS_DEL_LIST = ["Datasource", "Data_Value_Unit", "PriorityArea2", "PriorityArea3", "PriorityArea4",
               "Data_Value_Footnote_Symbol", "Data_Value_Footnote"]
SM_DEL_LIST = ["DataSource", "Data_Value_Unit", "Data_Value_Type", "Data_Value_Footnote_Symbol",
               "Data_Value_Footnote"]
DF_HS = DELETE.del_col(DF_HS, HS_DEL_LIST)
DF_SM = DELETE.del_col(DF_SM, SM_DEL_LIST)
print("The abundant columns are deleted from the datasets.")
pd.set_option('mode.chained_assignment', None)

RENAME = Rename()
DF_HS = RENAME.rename_col(DF_HS, {"Data_Value": "Heart Rate (%)"})
SM_DICT = {"Data_Value": "Smoking Rate (%)", "YEAR": "Year",
           "TopicType": "Category",
           "TopicDesc": "Topic", "Low_Confidence_Limit": "Confidence_Limit_Low",
           "High_Confidence_Limit": "Confidence_Limit_High"}
DF_SM = RENAME.rename_col(DF_SM, SM_DICT)
print("Some columns are renamed.")

print("Joining datasets...")
JOIN = Join()
DF_HS = JOIN.changet_to_type(DF_HS, ['Year'], str)
DF_SM = JOIN.changet_to_type(DF_SM, ['Year'], str)
DF_JOINED = JOIN.join_datasets(DF_HS, DF_SM, ['Year', 'LocationDesc'])
print("Datasets are joined.")

print("Applying filters on data ... (Please wait)")
OVERALL_DATA = OVERALL()
H_FILTERED = OVERALL_DATA.filter_hd(DF_JOINED)
ST_FILTERED = OVERALL_DATA.filter_st(DF_JOINED)
print("Dataset is filtered")

print("Preprocessing data ...")
DATA_PROCESS = DataFrames()
H_FILTERED = DATA_PROCESS.dropna(H_FILTERED, "Year")
H_FILTERED_BF = DATA_PROCESS.sort(H_FILTERED, "Year")
H_FILTERED = DATA_PROCESS.groupby_mean(H_FILTERED_BF, ['Year', 'LocationDesc'])
ST_FILTERED = DATA_PROCESS.dropna(ST_FILTERED, "Year")
ST_FILTERED_BF = DATA_PROCESS.sort(ST_FILTERED, "Year")
ST_FILTERED = DATA_PROCESS.groupby_mean(ST_FILTERED_BF, ['Year', 'LocationDesc'])

DF_SM_FILTERED = OVERALL_DATA.filter_sm(DF_SM)
DF_SM2 = DATA_PROCESS.groupby_mean(DF_SM_FILTERED, ['Year', 'LocationDesc'])
print("Plaase be patient ... (We are working on preprocessing the data)")
FM = FEMALE_MALE()
FEMALE_HEART = FM.filter_st_hd_fe_ma(DF_JOINED, "Coronary Heart Disease", "Female")
FEMALE_STROKE = FM.filter_st_hd_fe_ma(DF_JOINED, "Stroke", "Female")
MALE_HEART = FM.filter_st_hd_fe_ma(DF_JOINED, "Coronary Heart Disease", "Male")
MALE_STROKE = FM.filter_st_hd_fe_ma(DF_JOINED, "Stroke", "Male")
FEMALE_HEART = DATA_PROCESS.dropna(FEMALE_HEART, "Year")
FEMALE_STROKE = DATA_PROCESS.dropna(FEMALE_STROKE, "Year")
MALE_HEART = DATA_PROCESS.dropna(MALE_HEART, "Year")
MALE_STROKE = DATA_PROCESS.dropna(MALE_STROKE, "Year")
print("The data is preprocessed.")
print("Now, Please Maximize the Python Application from the Taskbar.")

width = GetSystemMetrics(0)
height = GetSystemMetrics(1)

PLOTS = Plots()
FM = FEMALE_MALE()

app = SeaofBTCapp()
app.mainloop()
