# The code for changing pages was derived from: http://stackoverflow.com/questions/7546050/switch-between-two-frames-in-tkinter
# License: http://creativecommons.org/licenses/by-sa/3.0/

import matplotlib
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import tkinter as tk
from tkinter import ttk
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb
import statsmodels.api as sm
import scipy
from sklearn.linear_model import LinearRegression
from tkinter.ttk import *
from PIL import Image
from PIL import ImageTk
from win32api import GetSystemMetrics

matplotlib.use("TkAgg")

LARGE_FONT = ("Verdana", 12)

__author__ = "zahra jalili"

"""
------------------------------------------------------------------------
                 Functionality Classes and Methods
------------------------------------------------------------------------
"""


class Read:
    @staticmethod
    def read_csv_file(filename):
        DF1 = pd.read_csv(filename)
        return DF1


class Delete:
    @staticmethod
    def del_col(df, deletedList):
        # deleting some of the columns of heart
        for item in deletedList:
            del df[item]
        return df


class Rename:
    @staticmethod
    def rename_col(df, rename_dict):
        # Rename a column name for heart
        for item in rename_dict:
            df = df.rename(columns={item: rename_dict[item]})
        return df


class Join:
    # join two df
    @staticmethod
    def changet_to_type(df, col_list, type):
        # change the type of column Year for ech datafarmes
        for item in col_list:
            df[item] = df[item].astype(type)
        return df

    @staticmethod
    def join_datasets(df1, df2, col_list):
        # use inner join for merging two datasets
        return pd.merge(left=df1, right=df2, left_on=col_list, right_on=col_list)


class OVERALL:

    ################## OVERALL
    @staticmethod
    def filter_hd(df):
        df1 = df[(df["Data_Value_Type"] == "Crude") & (df["Break_out"] == "Overall") &
                 (df["Category_x"] == "Cardiovascular Diseases") & (df["Topic_x"] == "Coronary Heart Disease")]

        return df1

    @staticmethod
    def filter_st(df):
        df1 = df[(df["Data_Value_Type"] == "Crude") & (df["Break_out"] == "Overall") &
                 (df["Category_x"] == "Cardiovascular Diseases") & (df["Topic_x"] == "Stroke")]

        return df1

    @staticmethod
    def filter_sm(df):
        df1 = df[((df["Year"] == "2011") | (df["Year"] == "2012") | (df["Year"] == "2013") | (df["Year"] == "2014")
                  | (df["Year"] == "2015")) & (df["Race"] == "All Races") & (df["Gender"] == "Overall")
                 ]
        return df1


class DataFrames:
    @staticmethod
    def dropna(df, column):
        df.dropna(subset=[column], inplace=True)
        return df

    @staticmethod
    def sort(df, column):
        df.sort_values(by=[column], inplace=True)
        return df

    @staticmethod
    def groupby_mean(df, columns_list):
        df = df.groupby(columns_list).mean()
        return df


class Plots:
    @staticmethod
    def plot_year(df, column, title, y_label, color):
        f = Figure(figsize=(5, 5), dpi=100)
        a = [f.add_subplot(511), f.add_subplot(512), f.add_subplot(513),
             f.add_subplot(514), f.add_subplot(515)]
        df[column].unstack(level=0).plot(kind='bar', subplots=True, legend=True,
                                         cmap=color,
                                         title=title,
                                         ax=a)
        # setting the y label for each plot
        count = 0
        for plot in a:
            plot.set_ylabel(y_label)
            if count != 4:
                plot.get_xaxis().set_visible(False)
            count += 1
        return f

    @staticmethod
    def plot_regression(df, x_column, y_column, title, y_label):
        # Create the scatter plot with a regression line
        f = Figure(figsize=(5, 5), dpi=100)
        a = f.add_subplot(111)
        sb.regplot(x=x_column, y=y_column,
                   data=df, scatter_kws={"color": "black"}, line_kws={"color": "red"}, ax=a)
        a.title.set_text(title)
        a.set_ylabel(y_label)
        return f

    @staticmethod
    def plot_regplot_sidebyside(df1, df2, x_column1, y_column1, x_column2, y_column2,
                                title1, title2, y_label):
        fig, axs = plt.subplots(ncols=2)
        sb.regplot(x=x_column1, y=y_column1,
                   data=df1[[x_column1, y_column1]], scatter_kws={"color": "blue"}, line_kws={"color": "green"},
                   ax=axs[0])
        axs[0].title.set_text(title1)
        # axs[0].set_ylim(1.5, 9)
        axs[0].set_xlim(6, 35)
        axs[0].set_ylabel(y_label)
        sb.regplot(x=x_column2, y=y_column2,
                   data=df2[[x_column2, y_column2]], scatter_kws={"color": "green"}, line_kws={"color": "red"},
                   ax=axs[1])
        axs[1].title.set_text(title2)
        # axs[1].set_ylim(1.5, 9)
        axs[1].set_xlim(6, 35)
        axs[1].set_ylabel(y_label)
        return fig

    @staticmethod
    def line_plot(df, x_column, y_column, title):
        sb.lineplot(df[x_column], df[y_column])
        plt.title(title)
        plt.show()

    @staticmethod
    def regression_slope2(x, y):
        slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x.tolist(), y.tolist())
        # For retrieving the slope:
        return slope


class FEMALE_MALE:
    @staticmethod
    def filter_st_hd_fe_ma(df, topic_x, sex):
        df1 = df[(df["Data_Value_Type"] == "Crude") & (df["Break_out"] == sex) &
                 (df["Gender"] == sex) & (df["Topic_x"] == topic_x) & (df["Race"] == "All Races") &
                 (df["Age"] == "All Ages") & (df["Education"] == "All Grades") & (df["Response"] == "Current")]
        return df1
