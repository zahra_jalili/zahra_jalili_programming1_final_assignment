Author: Zahra Jalili  
All Rights Reserved.  
February 2020.   
----------------------------------------------------------------  
This project plots different visualizations for investigating the effect  
of smoking on heart disease and storke in all states of US from 2011 to 2015.  
It also proves that smoking has worst effect on females than males.  
-----------------------------------------------------------------  

## Install:  
In order to be able to use this software please install the following libraries first.   
1. numpy  
2. pandas  
3. seaborn  
4. statsmodels  
5. scipy  
6. matplotlib  
7. PIL  
8. win32api  

## Graphical User Interface (GUI)  
The main part of the developed application is included in GUI file.   
Please run GUI.py to see all the graphs and visualizations. 

## Unit testing  
The functionality of application has been tested by unit testing.   
In order to do the unittesting please run the unit_test_functionalities.py file.   

 
  
  
After installing the aforementioned libraries you can easily run the app.